#!/usr/bin/env python
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

"""
Add label to merge request
"""
import argparse
import logging

import gitlab
from gitlab.exceptions import GitlabError


def main():
  parser = argparse.ArgumentParser(description="GitLab auto label MR",
                                   formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("-i", "--gitlabMergeRequestIid", required=True, help="Gitlab Merge Request Iid")
  parser.add_argument("-p", "--project-name", dest="project_name", required=True,
                      help="GitLab project with namespace (e.g. user/my-project)")
  parser.add_argument("-t", "--token", required=True, help="private GitLab user token")
  parser.add_argument("-l", "--label", required=True, help="Label to be added to merge request")
  parser.add_argument("--url", default="https://gitlab.cern.ch", help="URL of GitLab instance")
  parser.add_argument("-v", "--verbose", default="DEBUG",
                      choices=[
                          "DEBUG",
                          "INFO",
                          "WARNING",
                          "ERROR",
                          "CRITICAL"],
                      help="verbosity level")

  # get command line arguments
  args = parser.parse_args()

  logging.debug("parsed arguments:")
  for name, value in vars(args).items():
    logging.debug("    %12s : %s", name, value)

  # get GitLab API handler
  gl = gitlab.Gitlab(args.url, args.token, api_version=4)
  try:
    # get Gitlab project object
    project = gl.projects.get(args.project_name)
    logging.debug("retrieved Gitlab project handle")
  except GitlabError as e:
    logging.critical("error communication with Gitlab API '%s'", e.error_message)
    sys.exit(1)

  try:
    # get the MR that triggered the hook
    MR = project.mergerequests.get(args.gitlabMergeRequestIid)
    logging.debug("retrieved Gitlab MR for Iid %s", args.gitlabMergeRequestIid)
  except GitlabError as e:
    logging.critical("error communication with Gitlab API '%s'", e.error_message)
    sys.exit(1)

  # Add label to current labels if not already there
  mrLabels = set(MR.labels)
  mrLabels.add(args.label)
  MR.labels = list(mrLabels)
  MR.save()

  return None


if __name__ == "__main__":
  main()
